// Теоретичні питання
// 1.Описати своїми словами навіщо потрібні функції у програмуванні.
// Для легкого використання частини коду в рызних мысцях програми.
// 2.Описати своїми словами, навіщо у функцію передавати аргумент.
// Передаючи аргументи, функція приймає іх як парметрию. Потім вже виконує дію, які нам потрібні з цими значеннями.
// 3.Що таке оператор return та як він працює всередині функції?
// повертає результат значення. Записується в кінці фунції та повертає кінцевий результат(результатом може буде змінна або сама дія).


// Завдання
// Реалізувати функцію, яка виконуватиме математичні операції з введеними користувачем числами. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:
// Отримати за допомогою модального вікна браузера два числа.
// Отримати за допомогою модального вікна браузера математичну операцію, яку потрібно виконати. Сюди може бути введено +, -, *, /.
// Створити функцію, в яку передати два значення та операцію.
// Вивести у консоль результат виконання функції.
// Необов'язкове завдання підвищеної складності
// Після введення даних додати перевірку їхньої коректності. Якщо користувач не ввів числа, або при вводі вказав не числа, - запитати обидва числа знову (при цьому значенням за замовчуванням для кожної зі змінних має бути введена інформація раніше)

let firstElem = + prompt('Write the first number', 'number');
if (!Number.isInteger(firstElem) || firstElem === ''){
    do {
      alert('Write the first number')
      firstElem = + prompt('Write the first number', firstElem)
    } while (!Number.isInteger(firstElem))
  }
// console.log(firstElem);

let secondElem = + prompt('Write the second number', 'number');
if (!Number.isInteger(secondElem) || secondElem === ''){
    do {
      alert('Write the second number')
      secondElem = + prompt('Write the second number', secondElem)
    } while (!Number.isInteger(secondElem))
  }
// console.log(secondElem);

let mathOperator = prompt('Write the math operator', 'math operator');
if (mathOperator != '+' && mathOperator != '-' && mathOperator != '*' && mathOperator !='/') {
    do {
        alert('Write the math operator')
        mathOperator = prompt('Write the math operator', mathOperator);
    } while (mathOperator != '+' && mathOperator != '-' && mathOperator != '*' && mathOperator !='/')
  }
// console.log(mathOperator);

let result;
function mathOperation(firstElem, mathOperator, secondElem){
    if (mathOperator === '+'){
        result = firstElem + secondElem;
        return result;
    }
    else if (mathOperator === '-'){
        result = firstElem * secondElem;
        return result;
    } 
    else if (mathOperator === '*'){
        result = firstElem * secondElem;
        return result;
    }
    else if (mathOperator === '/'){
        result = firstElem / secondElem;
        return result;
    }
}
mathOperation(firstElem, mathOperator, secondElem);
console.log(result);


